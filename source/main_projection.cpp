#define _CRT_SECURE_NO_WARNINGS

/******************************/
// http server
#include <cpprest/http_listener.h>
#include <cpprest/json.h>

#ifdef _DEBUG
#pragma comment(lib, "cpprest140d_2_9")
#else
#pragma comment(lib, "cpprest140_2_9")
#endif

using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

#include <iostream>
#include <map>
#include <set>
#include <string>
using namespace std;

#include <tinyxml.h>
#pragma comment(lib, "tinyxml.lib")

#define TRACE(msg)            wcout << msg
#define TRACE_ACTION(a, k, v) wcout << a << L" (" << k << L", " << v << L")\n"
// http server
/******************************/

#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include <locale>
#include <codecvt>
#include <cstdio>
#include <ctime>

#include "CCreateProjImage.h"
#include "CWindowMngr.h"
#include <windows.h>
#include <GL/glut.h>
#pragma comment(lib, "freeglut.lib")

CCreateProjImage *ptrCCreateProjImage;
CWindowMngr *ptrCWindowMngr;

bool projectionFlag;
int intensity;
int changeFlag;
int movieID;
int duration;
std::time_t t0;

/******************************/
/* http */
/******************************/
map<utility::string_t, utility::string_t> dictionary;
std::string url;

void display_json(
	json::value const & jvalue,
	utility::string_t const & prefix)
{
	wcout << prefix << jvalue.serialize() << endl;
}

void handle_get(http_request request)
{
	TRACE(L"\nhandle GET\n");

	auto answer = json::value::object();

	for (auto const & p : dictionary)
	{
		answer[p.first] = json::value::string(p.second);
	}

	display_json(json::value::null(), L"R: ");
	display_json(answer, L"S: ");

	request.reply(status_codes::OK, answer);
}

void handle_request(
	http_request request,
	function<void(json::value const &, json::value &)> action)
{
	auto answer = json::value::object();

	request
		.extract_json()
		.then([&answer, &action](pplx::task<json::value> task) {
		try
		{
			auto const & jvalue = task.get();
			display_json(jvalue, L"R: ");

			if (!jvalue.is_null())
			{
				action(jvalue, answer);
			}
		}
		catch (http_exception const & e)
		{
			wcout << e.what() << endl;
		}
	})
		.wait();


	display_json(answer, L"S: ");

	request.reply(status_codes::OK, answer);
}

void handle_post(http_request request)
{
	TRACE("\nhandle POST\n");

	handle_request(
		request,
		[](json::value const & jvalue, json::value & answer)
	{
		for (auto const & e : jvalue.as_array())
		{
			if (e.is_string())
			{
				auto key = e.as_string();
				auto pos = dictionary.find(key);

				if (pos == dictionary.end())
				{
					answer[key] = json::value::string(L"<nil>");
				}
				else
				{
					answer[pos->first] = json::value::string(pos->second);
				}
			}
		}
	});
}
bool tmp_changeFlag;
void handle_put(http_request request)
{
	TRACE("\nhandle PUT\n");
	tmp_changeFlag = false;

	handle_request(
		request,
		[](json::value const & jvalue, json::value & answer)
	{
		for (auto const & e : jvalue.as_object())
		{
			if (e.second.is_string())
			{
				auto key = e.first;
				auto value = e.second.as_string();

				if (dictionary.find(key) == dictionary.end())
				{
					TRACE_ACTION(L"added", key, value);
					answer[key] = json::value::string(L"<put>");
				}
				else
				{
					TRACE_ACTION(L"updated", key, value);
					answer[key] = json::value::string(L"<updated>");
				}

				dictionary[key] = value;

				/////// daisuke /////////
				std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
				std::string skey = cv.to_bytes(key);
				std::string svalue = cv.to_bytes(value);

				if (!skey.compare("degree")) {
					intensity = atoi(svalue.c_str()) / 10 - 1;
				}
				if (!skey.compare("time")) {
					duration = atoi(svalue.c_str());
				}
				if (!skey.compare("type")) {
					if (!svalue.compare("rain")) {
						movieID = ptrCCreateProjImage->RAIN;
					}
					if (!svalue.compare("flower")) {
						movieID = ptrCCreateProjImage->FLOWER;
					}
					if (!svalue.compare("stop")) {
						intensity = -1;
					}
					tmp_changeFlag = true;
				}
			}
		}
		changeFlag = tmp_changeFlag;

	});
}

void handle_del(http_request request)
{
	TRACE("\nhandle DEL\n");

	handle_request(
		request,
		[](json::value const & jvalue, json::value & answer)
	{
		set<utility::string_t> keys;
		for (auto const & e : jvalue.as_array())
		{
			if (e.is_string())
			{
				auto key = e.as_string();

				auto pos = dictionary.find(key);
				if (pos == dictionary.end())
				{
					answer[key] = json::value::string(L"<failed>");
				}
				else
				{
					TRACE_ACTION(L"deleted", pos->first, pos->second);
					answer[key] = json::value::string(L"<deleted>");
					keys.insert(key);
				}
			}
		}

		for (auto const & key : keys)
			dictionary.erase(key);
	});
}

void getURL(char *xmlfname) {
	TiXmlDocument doc_cp(xmlfname);
	if (!doc_cp.LoadFile()) {
		std::cout << xmlfname << " does not exist.\n";
		exit(1);
	}
	else {
		TiXmlNode*		rootNode = 0;
		TiXmlElement*	rootElement = 0;
		rootNode = doc_cp.RootElement();
		if (!((rootNode != 0) && (rootNode->ToElement()))) {
			std::cout << "This is not a valid xml file\n";
			exit(1);
		}
		if (strcmp("http", rootNode->Value()) != 0) {
			std::cout << "This is not a valid http xml file\n";
			exit(1);
		}
		url = rootNode->ToElement()->Attribute("url");
	}
	std::cout << url;
}

void serverSetup()
{
	//http_listener listener(L"http://localhost/api/vr");
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
	http_listener listener(cv.from_bytes(url));

	listener.support(methods::GET, handle_get);
	listener.support(methods::POST, handle_post);
	listener.support(methods::PUT, handle_put);
	listener.support(methods::DEL, handle_del);

	try
	{
		listener
			.open()
			.then([&listener]() {TRACE(L"\nstarting to listen\n"); })
			.wait();

		while (true);
	}
	catch (exception const & e)
	{
		wcout << e.what() << endl;
	}
}

/******************************/
/* GLUT Callback function - display */
/******************************/

void Display( void )
{
	if (changeFlag) {
		if (intensity == -1) { 
			projectionFlag = false; 
		}else {
			ptrCCreateProjImage->bindTexture(intensity, movieID);
			projectionFlag = true;
			t0 = std::time(0);
		}
		changeFlag = false;
	}

	std::time_t now = std::time(0);
	if (projectionFlag && now - t0 > duration) {
		changeFlag = true;
		intensity = -1;
	}

	/******************************/
	/* clear buffers & init matrices */
	/******************************/
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0, 0, 0, 0);

	glViewport(0, 0, ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH());
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH(), 0);
	glMatrixMode(GL_MODELVIEW);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glLineWidth(1.0);
	glPointSize(1.0);

	/******************************/
	/* display texture image      */
	/******************************/
	if (projectionFlag) {
		ptrCCreateProjImage->projTexture();
		Sleep(30);
	}

	/******************************/
	/* swap buffers               */
	/******************************/
	glutSwapBuffers();
	glutPostRedisplay();
}

/******************************/
/* GLUT Callback function - keyboard */
/******************************/

void Keyboard( unsigned char key, int x, int y )
{
	switch( key )
	{
	case '0':	intensity = 0;	break;
	case '1':	intensity = 1;	break;
	case '2':	intensity = 2;	break;
	case '3':	intensity = 3;	break;
	case '4':	intensity = 4;	break;
	case '5':	intensity = 5;	break;
	case '6':	intensity = 6;	break;
	case '7':	intensity = 7;	break;
	case '8':	intensity = 8;	break;
	case '9':	intensity = 9;	break;
	case 'r':	movieID = ptrCCreateProjImage->RAIN;	break;
	case 'f':	movieID = ptrCCreateProjImage->FLOWER;	break;
	case 27:
		exit(1);
		break;
	default:
		break;
	}

	changeFlag = true;

}

/******************************/
/* main function - entry point */
/******************************/

int main( int argc, char **argv )
{
	if( argc != 4 ){
		std::cout << "Specify window information xml, corner position xml, http xml files.\n";
		std::cout << "e.g., ./Projection.exe windowInfo.xml cornerPos.xml http.xml\n";
		std::cout << "Press enter key to quit.\n";
		getchar();
		exit(1);
	}
	getURL(argv[3]);
	std::thread threadServer(serverSetup);
	changeFlag = false;

	/******************************/
	/* init GLUT                  */
	/******************************/
	ptrCWindowMngr = new CWindowMngr(argv[1]);
	glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
//	glutInitWindowPosition(ptrCWindowMngr->getInitWindowPosX(), ptrCWindowMngr->getInitWindowPosY()	);
	glutInitWindowSize( ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH() );
	glutCreateWindow( "Projection Experiment" );

	/******************************/
	/* GLUT callbacks             */
	/******************************/
	glutDisplayFunc( Display );
	glutKeyboardFunc( Keyboard );

	/******************************/
	/* init create projection image*/
	/******************************/
	intensity = -1;
	duration = 0;
	projectionFlag = false;
	movieID = ptrCCreateProjImage->FLOWER;
	t0 = std::time(0);
	ptrCCreateProjImage = new CCreateProjImage( argv[2], intensity, movieID );

	/******************************/
	/* windows api              */
	/******************************/
	HDC glDc = wglGetCurrentDC();
	HWND hWnd = WindowFromDC(glDc);
	SetWindowLong(hWnd, GWL_STYLE, WS_POPUP);
	SetWindowPos(hWnd, HWND_TOP, ptrCWindowMngr->getInitWindowPosX(), ptrCWindowMngr->getInitWindowPosY(), ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH(), SWP_SHOWWINDOW);

	/******************************/
	/* GLUT mainloop              */
	/******************************/
	glutMainLoop();

	/******************************/
	/* never reach here           */
	/******************************/
	return 0;
}
