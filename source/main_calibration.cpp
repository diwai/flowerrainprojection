#include <iostream>
#include <windows.h>
#include <GL/glut.h>
#pragma comment(lib, "freeglut.lib")

#include "CWindowMngr.h"
#include "CCornerCalibMngr.h"

CCornerCalibMngr *ptrCCornerCalibMngr;
CWindowMngr *ptrCWindowMngr;
int cornerID = 0;

void Display( void )
{
	/******************************/
	/* clear buffers & init matrices */
	/******************************/
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glClearColor( 0, 0, 0, 0 );

	glViewport(0, 0, ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH());
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluOrtho2D( 0, ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH(), 0 );
	glMatrixMode( GL_MODELVIEW );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	glLineWidth( 1.0 );
	glPointSize( 1.0 );

	/******************************/
	/* display calib rectangle    */
	/******************************/
	ptrCCornerCalibMngr->disp();

	// swap buffers
    glutSwapBuffers();
	glutPostRedisplay();
}

void Keyboard( unsigned char key, int x, int y )
{
	FILE *fp;
	switch( key )
	{
	case '0':
	case '1':
	case '2':
	case '3':
		cornerID = atoi((char*)&key);
		break;
	case 's':
		ptrCCornerCalibMngr->saveCornerPos();
		break;
	case 27:
		ptrCCornerCalibMngr->saveCornerPos();
		exit(1);
		break;
	}
}

void Mouse( int button, int state, int x, int y )
{
	if( state == GLUT_DOWN )
	{
		ptrCCornerCalibMngr->setCornerPos( cornerID, x, y );
	}
}

int main( int argc, char **argv )
{
	int i;
	char str[20];

	/******************************/
	/* init disp calibration result*/
	/******************************/
	if( argc != 3 ){
		std::cout << "Specify window information xml and corner position xml files.\n";
		std::cout << "e.g., ./Calibration.exe windowInfo.xml cornerPos.xml\n";
		std::cout << "Press enter key to quit.\n";
		getchar();
		exit(1);
	}else{
		ptrCWindowMngr = new CWindowMngr(argv[1]);
		ptrCCornerCalibMngr = new CCornerCalibMngr(argv[2]);
	}

	/******************************/
	/* init GLUT                  */
	/******************************/
	glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	//glutInitWindowPosition(ptrCWindowMngr->getInitWindowPosX(), ptrCWindowMngr->getInitWindowPosY());
	glutInitWindowSize( ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH() );
	glutCreateWindow( "Projector Calibration" );

	/******************************/
	/* GLUT callbacks             */
	/******************************/
	glutDisplayFunc( Display );
	glutKeyboardFunc( Keyboard );
	glutMouseFunc( Mouse );

	/******************************/
	/* windows api              */
	/******************************/
	HDC glDc = wglGetCurrentDC();
	HWND hWnd = WindowFromDC(glDc);
	SetWindowLong(hWnd, GWL_STYLE, WS_POPUP);
	SetWindowPos(hWnd, HWND_TOP, ptrCWindowMngr->getInitWindowPosX(), ptrCWindowMngr->getInitWindowPosY(), ptrCWindowMngr->getWindowW(), ptrCWindowMngr->getWindowH(), SWP_SHOWWINDOW);

	/******************************/
	/* GLUT mainloop              */
	/******************************/
	glutMainLoop();

	/******************************/
	/* never reach here           */
	/******************************/
	return 0;
}
