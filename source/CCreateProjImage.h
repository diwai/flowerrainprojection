﻿#pragma once

#include <iostream>
#include <vector>
#include <random>

#include <opencv2/opencv.hpp>
#define CV_VERSION_STR CVAUX_STR(CV_MAJOR_VERSION) CVAUX_STR(CV_MINOR_VERSION) CVAUX_STR(CV_SUBMINOR_VERSION)
#ifdef _DEBUG
#define CV_EXT_STR "d.lib"
#else
#define CV_EXT_STR ".lib"
#endif
#pragma comment(lib, "opencv_world" CV_VERSION_STR CV_EXT_STR)

#include <GL/glew.h>
#include <GL/glut.h>
#include <tinyxml.h>
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "freeglut.lib")
#pragma comment(lib, "tinyxml.lib")

#define textureW 1920
#define textureH 1080

const char rainfname[] = "../data/rain.mov";
const char raindir[] = "../data/rain/";
const char flowerfname[] = "../data/flower.jpg";
const int rainFrame[10] = { 40,240,440,620,860,1050,1250,1430,1650,1850 };
const int rainDurationInFrame = 30;
const int flowerSize = 500;
const int flowerSizeT = 100;
const int intensityFactorFlower = 70;

class CCreateProjImage {

private:
	cv::Mat Tex2PrjHomMat;
	std::vector<GLuint> texID;
	int textureID;
	int movieID;
	cv::Mat flowerImg;

public:
	enum {RAIN, FLOWER};

public:
	CCreateProjImage( char *fname_cornerPos, int intensity, int movieID )
	{
		// read corner positions
		int corner[4][4];
		TiXmlDocument doc_cp( fname_cornerPos );
		if( !doc_cp.LoadFile() ){
			std::cout << fname_cornerPos << " does not exist.\n";
			std::cout << "Press enter key to quit.\n";
			getchar();
			exit(1);
		}else{
			TiXmlNode*		rootNode = 0;
			TiXmlElement*	rootElement = 0;
			rootNode = doc_cp.RootElement();
			if( !( (rootNode!=0) && (rootNode->ToElement()) ) ){
				std::cout << "This is not a valid corner position file\n";
				std::cout << "Press enter key to quit.\n";
				getchar();
				exit(1);
			}
			if( strcmp( "Corner_Position", rootNode->Value() ) != 0 ){
				std::cout << "This is not a valid corner position file\n";
				std::cout << "Press enter key to quit.\n";
				getchar();
				exit(1);
			}
			TiXmlNode *rootChild = 0;
			while( rootChild = rootNode->IterateChildren( rootChild ) ){
				// iterate over all possible objects
				if( strcmp( rootChild->Value(), "top_left" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[0][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[0][1]);
				}
				if( strcmp( rootChild->Value(), "top_right" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[1][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[1][1]);
				}
				if( strcmp( rootChild->Value(), "bottom_right" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[2][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[2][1]);
				}
				if( strcmp( rootChild->Value(), "bottom_left" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[3][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[3][1]);
				}
			}
		}

		// alloc homography matrix and set corner positions in projection image
		cv::Mat prjMat(4, 2, CV_64FC1);
		cv::Mat texMat(4, 2, CV_64FC1);
		for (int i = 0; i < 4; i++) {
			prjMat.at<double>(i, 0) = corner[i][0];
			prjMat.at<double>(i, 1) = corner[i][1];
		}
		texMat.at<double>(0, 0) = 0;
		texMat.at<double>(0, 1) = 0;
		texMat.at<double>(1, 0) = textureW;
		texMat.at<double>(1, 1) = 0;
		texMat.at<double>(2, 0) = textureW;
		texMat.at<double>(2, 1) = textureH;
		texMat.at<double>(3, 0) = 0;
		texMat.at<double>(3, 1) = textureH;
		Tex2PrjHomMat = cv::findHomography(texMat, prjMat);

		// read flower texture
		flowerImg = cv::imread(flowerfname);

		// bind textures
		bindTexture(intensity, movieID);
	}
	~CCreateProjImage()
	{
	}

	//int getNumberOfTexture( void ){
	//	return number_of_texture;
	//}

	void bindTexture( int intensity, int inMovieID ) {
		for (int i = 0; i < texID.size(); i++) {
			glDeleteTextures(1, &texID.at(i));
		}
		texID.clear();

		movieID = inMovieID;

		if (movieID == FLOWER) {
			cv::Mat texImage = cv::Mat::zeros(cv::Size(textureW, textureH), CV_8UC3);

			std::random_device rd;
			std::mt19937 mt(rd());
			std::uniform_int_distribution<int> fx(0, flowerImg.cols / flowerSize - 1);
			std::uniform_int_distribution<int> fy(0, flowerImg.rows / flowerSize - 1);
			std::uniform_int_distribution<int> tx(0, textureW - flowerSizeT - 1);
			std::uniform_int_distribution<int> ty(0, textureH - flowerSizeT - 1);
			for (int i = 0; i < (intensity+1) * intensityFactorFlower; i++) {
				cv::Rect roif(fx(mt)*flowerSize, fy(mt)*flowerSize, flowerSize, flowerSize);
				cv::Rect roit(tx(mt), ty(mt), flowerSizeT, flowerSizeT);
				cv::Mat f_roi = flowerImg(roif);
				cv::Mat t_roi = texImage(roit);
				cv::Mat f_roi2 = cv::Mat::zeros(cv::Size(flowerSize, flowerSize), CV_8UC3);
				cv::Mat f_roi3 = cv::Mat::zeros(cv::Size(flowerSizeT, flowerSizeT), CV_8UC3);
				f_roi.copyTo(f_roi2);
				cv::resize(f_roi2, f_roi3, f_roi3.size(), cv::INTER_CUBIC);
				for (int r = 0; r < f_roi3.rows; r++) {
					for (int c = 0; c < f_roi3.cols; c++) {
						if ((unsigned char)f_roi3.at<cv::Vec3b>(r, c)[0] != 0 || 
							(unsigned char)f_roi3.at<cv::Vec3b>(r, c)[1] != 0 || 
							(unsigned char)f_roi3.at<cv::Vec3b>(r, c)[2] != 0) {
							t_roi.at<cv::Vec3b>(r, c)[0] = f_roi3.at<cv::Vec3b>(r, c)[0];
							t_roi.at<cv::Vec3b>(r, c)[1] = f_roi3.at<cv::Vec3b>(r, c)[1];
							t_roi.at<cv::Vec3b>(r, c)[2] = f_roi3.at<cv::Vec3b>(r, c)[2];
						}
					}
				}


			}

			//cv::imshow("video", texImage);
			//cv::waitKey(0);

			// set GL texture
			GLuint tmp_texID;
			glGenTextures(1, &tmp_texID);
			texID.push_back(tmp_texID);
			glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tmp_texID);
			glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGB, textureW, textureH, 0, GL_BGR, GL_UNSIGNED_BYTE, texImage.data);

		}

		if (movieID == RAIN) {
			cv::Mat texImage = cv::Mat::zeros(cv::Size(textureW, textureH), CV_8UC3);
#if 0

#if 0
			int img_num = 0;
			cv::VideoCapture cap_img(rainfname);
			while (1) {
				/* read texture image */
				cv::Mat temp;
				cap_img >> temp;
				if (temp.empty())	break;

				cv::resize(temp, texImage, texImage.size());

				//if (img_num % 10 == 0) {
					std::ostringstream sout;
					sout << std::setfill('0') << std::setw(4) << img_num;
					std::string saveName1 = raindir + sout.str() + ".jpg";
					cv::imwrite(saveName1, texImage);
				//}
				std::cout << "frame: " << img_num++ << std::endl;
			}
#else
			for (int j = 0; j < 10; j++) {
				for (int i = 0; i < rainDurationInFrame; i++) {
					std::ostringstream sout, sin;
					sin << std::setfill('0') << std::setw(4) << i + rainFrame[j];
					std::string readName1 = raindir + sin.str() + ".jpg";
					cv::Mat temp = cv::imread(readName1);
					sout << "/" << j << "/" << std::setfill('0') << std::setw(4) << i;
					std::string readName2 = raindir + sout.str() + ".jpg";
					cv::imwrite(readName2, temp);
				}
			}
#endif
			exit(0);
#else
			for (int i = 0; i < rainDurationInFrame; i++) {
				std::ostringstream sout;
				sout << "/" << intensity << "/" << std::setfill('0') << std::setw(4) << i;
				std::string readName1 = raindir + sout.str() + ".jpg";
				cv::Mat temp = cv::imread(readName1);

				cv::resize(temp, texImage, texImage.size());
				//cv::imshow("video", texImage);
				//cv::waitKey(0);

				// set GL texture
				GLuint tmp_texID;
				glGenTextures(1, &tmp_texID);
				texID.push_back(tmp_texID);
				glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tmp_texID);
				glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
				glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGB, textureW, textureH, 0, GL_BGR, GL_UNSIGNED_BYTE, texImage.data);
			}
#endif
		}
		textureID = 0;
	}

	void projTexture( void )
	{
		glLoadIdentity();
		glTranslatef( 0, 0, 0.0 );
		glEnable(GL_TEXTURE_RECTANGLE_ARB);
		glBindTexture( GL_TEXTURE_RECTANGLE_ARB, texID.at(textureID) );

		GLfloat m[16];
		m[0] = Tex2PrjHomMat.at<double>(0, 0);
		m[1] = Tex2PrjHomMat.at<double>(1, 0);
		m[2] = 0.0;
		m[3] = Tex2PrjHomMat.at<double>(2, 0);
		//
		m[4] = Tex2PrjHomMat.at<double>(0, 1);
		m[5] = Tex2PrjHomMat.at<double>(1, 1);
		m[6] = 0.0;
		m[7] = Tex2PrjHomMat.at<double>(2, 1);
		//
		m[8] = 0.0;
		m[9] = 0.0;
		m[10] = 1.0;
		m[11] = 0.0;
		//
		m[12] = Tex2PrjHomMat.at<double>(0, 2);
		m[13] = Tex2PrjHomMat.at<double>(1, 2);
		m[14] = 0.0;
		m[15] = Tex2PrjHomMat.at<double>(2, 2);

		glMultMatrixf(m);

		glEnable(GL_TEXTURE_RECTANGLE_ARB);
		glBegin(GL_QUADS);
		glTexCoord2f( 0.0,	0.0 );			glVertex2f( 0.0,	0.0 );
		glTexCoord2f( textureW,	0.0 );		glVertex2f( textureW,	0.0 );
		glTexCoord2f( textureW,	textureH );	glVertex2f( textureW,	textureH );
		glTexCoord2f( 0.0,	textureH );		glVertex2f( 0.0,	textureH );
		glEnd();
		glDisable(GL_TEXTURE_RECTANGLE_ARB);

		if (movieID == RAIN) {
			textureID = (textureID+1)%rainDurationInFrame;
			//std::cout << textureID << " ";
		}
	}

};


