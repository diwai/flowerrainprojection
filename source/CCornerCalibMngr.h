#pragma once

#include <iostream>
#include <GL/glut.h>
#include <tinyxml.h>
#pragma comment(lib, "freeglut.lib")
#pragma comment(lib, "tinyxml.lib")

class CCornerCalibMngr {

private:
	int corner[4][2];
	char _fname[128];

public:

	CCornerCalibMngr( char *fname )
	{
		sprintf_s(_fname,"%s", fname);

		// read xml file
		TiXmlDocument doc( _fname );
		if( !doc.LoadFile() ){
			std::cout << "couldn't read " << _fname << std::endl;
			std::cout << "set default corner positions." << std::endl;
			corner[0][0] = 100;	corner[0][1] = 100;
			corner[1][0] = 200;	corner[1][1] = 100;
			corner[2][0] = 200;	corner[2][1] = 200;
			corner[3][0] = 100;	corner[3][1] = 200;
		}else{
			TiXmlNode*		rootNode = 0;
			TiXmlElement*	rootElement = 0;
			rootNode = doc.RootElement();
			if( !( (rootNode!=0) && (rootNode->ToElement()) ) ){
				std::cout << "This is not a valid file\n";
				exit(1);
			}
			if( strcmp( "Corner_Position", rootNode->Value() ) != 0 ){
				std::cout << "This is not a valid file\n";
				exit(1);
			}
			TiXmlNode *rootChild = 0;
			while( rootChild = rootNode->IterateChildren( rootChild ) ){
				// iterate over all possible objects
				if( strcmp( rootChild->Value(), "top_left" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[0][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[0][1]);
				}
				if( strcmp( rootChild->Value(), "top_right" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[1][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[1][1]);
				}
				if( strcmp( rootChild->Value(), "bottom_right" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[2][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[2][1]);
				}
				if( strcmp( rootChild->Value(), "bottom_left" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &corner[3][0]);
					rootChild->ToElement()->QueryIntAttribute("y", &corner[3][1]);
				}
			}
		}
	}
	~CCornerCalibMngr()
	{
	}

	void setCornerPos( int cornerID, int x, int y )
	{
		corner[cornerID][0] = x;
		corner[cornerID][1] = y;
	}

	int *getCornerPos( int cornerID )
	{
		return corner[cornerID];
	}

	void saveCornerPos( char *fname=NULL )
	{
		std::cout << "save corner positions to -> " << _fname << std::endl;

		TiXmlDocument doc;
		TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
		doc.LinkEndChild( decl );
		TiXmlElement *root = new TiXmlElement("Corner_Position");
		doc.LinkEndChild(root);
		TiXmlElement *elem_top_left = new TiXmlElement("top_left");
		root->LinkEndChild(elem_top_left);
		elem_top_left->SetAttribute("x", corner[0][0]);
		elem_top_left->SetAttribute("y", corner[0][1]);
		TiXmlElement *elem_top_right = new TiXmlElement("top_right");
		root->LinkEndChild(elem_top_right);
		elem_top_right->SetAttribute("x", corner[1][0]);
		elem_top_right->SetAttribute("y", corner[1][1]);
		TiXmlElement *elem_bottom_right = new TiXmlElement("bottom_right");
		root->LinkEndChild(elem_bottom_right);
		elem_bottom_right->SetAttribute("x", corner[2][0]);
		elem_bottom_right->SetAttribute("y", corner[2][1]);
		TiXmlElement *elem_bottom_left = new TiXmlElement("bottom_left");
		root->LinkEndChild(elem_bottom_left);
		elem_bottom_left->SetAttribute("x", corner[3][0]);
		elem_bottom_left->SetAttribute("y", corner[3][1]);
		doc.SaveFile(_fname);
	}

	void DrawString2d(const char *str,void *font,int x,int y)
	{
		glRasterPos2d(x,y);
		while(*str){
			glutBitmapCharacter(font, *str);
			++str;
		}
	}

	void disp( void )
	{
		/******************************/
		/* display calib rectangle    */
		/******************************/
		void *font = GLUT_BITMAP_HELVETICA_10;
		glLoadIdentity();

		glColor3d( 1.0,1.0,1.0 );
		glBegin( GL_LINE_STRIP );
		glVertex2iv( getCornerPos(0) );
		glVertex2iv( getCornerPos(1) );
		glVertex2iv( getCornerPos(2) );
		glVertex2iv( getCornerPos(3) );
		glVertex2iv( getCornerPos(0) );
		glEnd();

		// corner IDs
		glColor3d( 0.0,1.0,0.0 );
		DrawString2d("0: top left",font,corner[0][0],corner[0][1]);	
		DrawString2d("1: top right",font,corner[1][0],corner[1][1]);	
		DrawString2d("2: bottom right",font,corner[2][0],corner[2][1]);	
		DrawString2d("3: bottom left",font,corner[3][0],corner[3][1]);	
	}
};


