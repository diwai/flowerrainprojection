#pragma once

#include <iostream>
#include <tinyxml.h>
#pragma comment(lib, "tinyxml.lib")

class CWindowMngr {

private:
	int windowW;
	int windowH;
	int initWindowPosX;
	int initWindowPosY;

public:
	CWindowMngr( char *fname )
	{
		// read xml file
		TiXmlDocument doc( fname );
		if( !doc.LoadFile() ){
			std::cout << fname << " does not exist.\n";
			std::cout << "Press enter key to quit.\n";
			getchar();
			exit(1);
		}else{
			TiXmlNode*		rootNode = 0;
			TiXmlElement*	rootElement = 0;
			rootNode = doc.RootElement();
			if( !( (rootNode!=0) && (rootNode->ToElement()) ) ){
				std::cout << "This is not a valid window information file\n";
				exit(1);
			}
			if( strcmp( "Window_Information", rootNode->Value() ) != 0 ){
				std::cout << "This is not a valid window information file\n";
				exit(1);
			}
			TiXmlNode *rootChild = 0;
			while( rootChild = rootNode->IterateChildren( rootChild ) ){
				// iterate over all possible objects
				if( strcmp( rootChild->Value(), "window_size" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("w", &windowW);
					rootChild->ToElement()->QueryIntAttribute("h", &windowH);
				}
				if( strcmp( rootChild->Value(), "init_window_pos" ) == 0 ){
					rootChild->ToElement()->QueryIntAttribute("x", &initWindowPosX);
					rootChild->ToElement()->QueryIntAttribute("y", &initWindowPosY);
				}
			}
		}
	}
	~CWindowMngr(){}

	int getWindowW( void ){	 return windowW;	}
	int getWindowH( void ){	 return windowH;	}
	int getInitWindowPosX( void ){	return initWindowPosX;	}
	int getInitWindowPosY( void ){	return initWindowPosY;	}
};


